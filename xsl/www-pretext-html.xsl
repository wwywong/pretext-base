<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="../mathbook/xsl/pretext-html.xsl"/>

  <xsl:param name="html.knowl.theorem" select="'no'" />
  <xsl:param name="html.knowl.proof" select="'no'" />
  <xsl:param name="html.knowl.definition" select="'no'" />
  <xsl:param name="html.knowl.example" select="'no'" />
  <xsl:param name="html.knowl.project" select="'no'" />
  <xsl:param name="html.knowl.task" select="'no'" />
  <xsl:param name="html.knowl.list" select="'no'" />
  <xsl:param name="html.knowl.remark" select="'no'" />
  <xsl:param name="html.knowl.objectives" select="'no'" />
  <xsl:param name="html.knowl.outcomes" select="'no'" />
  <xsl:param name="html.knowl.figure" select="'no'" />
  <xsl:param name="html.knowl.table" select="'no'" />
  <xsl:param name="html.knowl.listing" select="'no'" />
  <xsl:param name="html.knowl.exercise.inline" select="'no'" />
  <xsl:param name="html.knowl.exercise.sectional" select="'no'" />
  <xsl:param name="html.knowl.exercise.worksheet" select="'no'" />
  <xsl:param name="html.knowl.exercise.readingquestion" select="'no'" />
</xsl:stylesheet>
