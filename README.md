# PreTeXt-base

Just a bunch of my personal customizations for using [PreTeXt](https://pretextbook.org/). The official PreTeXt repository is included as a Git submodule. (This means that when you clone this repository you either need to clone with `git clone --recurse-submodules`, or you need to run `git submodule init` on the `mathbook/` directory after cloning.)

## Vim-bindings

The folder `vim-bindings` contains some vim scripts to make editing PreTeXt XML files somewhat more pleasant. See the README file in the directory for more information. 

## Gitlab Pages deployment

This repository includes a `.gitlab-ci.yml` file which is configured to deploy, using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/), a public-facing website built from the file `inputs/main.xml`. 

Any static files (ones that does not need processing with `xsltproc`) should be placed in a separate `static/` directory, its contents will be copied over to the `public/` directory during the build.

If you don't want to use the pages feature: either remove the `.gitlab-ci.yml` file from your clone of this repository, or use a different file name for your project. (Though seriously: if that's the case, you should just clone the official repository instead of this one.)
